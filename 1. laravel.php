<?php
// Imagine that you have the luxury of making this code (taken from laravel application) perfect down to the smallest details. What kind of things would you want to change?
// Example of areas to consider: code formatting, naming conventions, encapsulation, potential simplifications, logic & performance issues, etc
// Once again - be as picky as you can be - something trivial as this class does not need to be perfect but yet - what prevents it from being so?

namespace Passcode\Ticket;

use Passcode\Common\Object;
use Passcode\QrCode\QrCodeGenerator;
use Doctrine\ORM\EntityManager;
use Passcode\Entity\Ticket;
use Illuminate\Routing\UrlGenerator;

/**
 * Description of CreateTicketService
 *
 * @author [removed]
 */
class CreateTicketService extends Object{
    
    
    /**
     *
     * @var QrCodeGenerator 
     */
    protected $qrCodeGenerator;
    
    /**
     *
     * @var EntityManager
     */
    protected $em;
    
    /**
     *
     * @var \Passcode\Signup\ConfirmMailer
     */
    protected $mailer;
    
    
    /**
     *
     * @var UrlGenerator
     */
    protected $urlGenerator;
    
    function __construct(QrCodeGenerator $qrCodeGenerator, EntityManager $em, \Passcode\Signup\ConfirmMailer $mailer, UrlGenerator $urlGenerator) {
        
        $this->qrCodeGenerator = $qrCodeGenerator;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->urlGenerator = $urlGenerator;
        
    }

            
    public function createTicket($userId,$eventId){
        
        $user = $this->em->getReference('pc:User', $userId);
        $event = $this->em->getReference('pc:Event', $eventId);
         
        $ticket = new Ticket();
        
        $ticket->event = $event;
        $ticket->user = $user;
        $ticket->token = $this->newToken();
        
        $tokenUrl =$this->urlGenerator->to('/ut/'.$ticket->token);
        
        $ticket->qrCode = $this->qrCodeGenerator->get($tokenUrl);

        $this->em->persist($ticket);
        $this->em->flush($ticket);
        
        $this->mailer->sendTicketBoughtEmail($ticket);
        
        return $ticket;
    }
    
    //This function will always be unique now and so there is no need for calls to the database to check uniqueness.
    protected function newToken(){
        
        $tokenString = str_replace('.', '', $_SERVER['REMOTE_ADDR']);
        $tokenString = trim($tokenString) == '' ? openssl_random_pseudo_bytes(4) : $tokenString;
        $tokenString = microtime() . $tokenString;
        $tokenString = dechex($tokenString);
        
        return $tokenString;
    }
    
    // I assume that newUniqueToken is only used in this class.  I was a bit hessitant of deleting it because it is public and is
    // potentially be used elsewhere.  I don't think it should be though.
}