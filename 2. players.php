<?php
/*
 The goal of this script is to keep the player roster up to date, by inserting new players that are not yet in the database and by updating existing players
 (to keep attributes such as team, weight, jersey number, etc up to date).

 Roster data is being kept in the `player` table with the following schema (abridged here for brevity):
 
 CREATE TABLE `player` (
  `player_id`int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `sport_id` tinyint(3)  NOT NULL DEFAULT '0',
  `position_id` smallint(5) NOT NULL DEFAULT '0',
  `team_id` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `cbs_id` int(11) NOT NULL DEFAULT '0',
  `xmlteam_id` int(11) NOT NULL DEFAULT '0',
  `created_dt` datetime NOT NULL,
);

 In the past, player roster updates were being done by pulling data from CBS sports - and thus each player had a unique cbs_id assigned to him. 
 However, the project has been recently switched to use data from a new vendor called XML Team and thus xmlteam_id has been added as the attribute.
 In addition, this script was created - to be used instead of the old one which was pulling data from CBS. Since XML Team is a new data source, 
 as of now xmlteam_id is equal to 0 for all the players - so the fact that we don't have certain xmlteam_id in the database does not mean that we
 don't have that specific player - we likely have him, it's just that he still needs his xmlteam_id set to the proper value. Once xmlteam_id is established, 
 we can always trust the data comming from XML team and update that vendor accordingly. However, until all players in the database have that attribute set, 
 the current logic has some issues:

  - db_field('player', 'player_id', db_m($player, 'first_name, last_name, sport_id, team_id')) attempts to determine player_id by checking database 
 for the first_name, last_name and team_id. If no matches are found, and there is no database record with a given xmlteam_id, a new player will be inserted
  - However, some players have outdated information and are currently playing for a different team than the one recorded in the database. 
  In cases like that, the current script will just assume that the given player does not exist in the database - which is likely not true
  - and if that is so, a duplicate record will be created, resulting in 2 different rows referencing the same physical person

 Your goal is to implement the following logic to prevent such duplicates (note - you do not need to worry about deleting existing duplicates - 
 just about making sure that new ones are not being inserted):
 1) if the player with xmlteam_id exists, update that player. We are assuming that whatever xmlteam_id player has is valid and should be trusted
 2) otherwise, check if the player with the given first_name, last_name, sport_id and team_id exists (thus merely lacking xmlteam_id) - if so, update that player
 (we are assuming that the chances of 2 guys with same names playeing in the same team are low enough that if it would ever happen, we would just update xmlteam_id 
 to correct values)
 3) otherwise, if only a single player with the same first_name, last_name and sport_id exists (note - no checking for the team_id), update that player
 (assuming that the chance of 2 new guys with identical names being added into the league on the same day are low enough for us not to worry about it)
 4) if no players with the given first_name, last_name and sport_id exist, insert a new player
 5) if more than one player with the given first_name, last_name and sport_id exist, call dbg() function and skip to the next player - 
 a human will need to review and make a call whether that player is indeed a new one or not (example - there are 2 players in NFL with the name of "Brandon Marshall" - but they are playing for 2 different teams. If the database has outdated information on which teams they belong to, automated script should not be trusted to make the call)

 As you can see, the code is already using some function based helpers, you will only need to use 5 of them - 2 of them are already used in the code:
  - dbg($string_describing_an_issue, $array_or_object_or_string_or_integer) is used to create a log entry 
  that gets revievied via the admin dashboard in addition to sending an alert email to the admin
  - db_rows($table, $columns_to_select, $where_statement) returns an array where each member is an array containing database row, as such:
      $players = db_rows('player', '*', 'player_id < 3'); // will return:
      array(
        1=>array('player_id'=>1, 'first_name'=>'Joe', [...]),
        2=>array('player_id'=>2, 'first_name'=>'Doe', [...]),
      );
  - db_exists($table, $where_statement) returns true if a match is found in database and false if it's not
      $player_exists = db_exists('player', 'player_id = ' . i($player_id));
  - i() prevents SQL injection by only allowing integers to be used, while stopping SQL statment from running and automatically calling dbg() otherwise
  - s() does simila filtering for strings (by aplying mysqli_real_escape_string)


 P.S.: the following code was taken out of production and abridged, thus skipping any includes and bootstrap code
 P.P.S.: please remove any code that you end up commenting out - that improves readability for both me and you

*/ 

// XML Team provides with several files each day - need to read all of them and process changes
$files = \Application\Modules\XMLTeam\XMLTeam::latestFiles($files, 2);
foreach ($files as $file) {

    // extract the player list out of the file
    $F = new \Application\Modules\FileSystem\File($file);
    $data = $F->readContents();
    $Xml = new SimpleXMLElement($data);
    $Players = $Xml->{'statistic'}->{'team'}->children();
    for ($i = 0; $i < count($Players->player); $i++) {

        $player_data = $Players->player[$i];
        
        $player_atts = $player_data->{'player-metadata'}->attributes();
        $player_name = $player_data->{'player-metadata'}->{'name'}->attributes();
        $team_name = $player_data->{'team'}->{'name'};
        
        $player = array();
        $player['first_name'] = $player_name['first'][0]->__toString();
        $player['last_name'] = $player_name['last'][0]->__toString();
        $player['sport_id'] = $Sport->id;
        $player['xmlteam_id'] = XmlTeamSlice::getXmlPid_str($player_atts['player-key']);
        $player['team_id'] = db_field('team', 'team_id', "description = '" . s($team_name) . "'");
        $player['created_dt'] = dt_now();
        
        $condition[] = array();
        $condition[2] = 'first_name = ' . s(i($player['first_name'])) .
        ' AND last_name = ' . s(i($player['last_name'])) .
        ' AND sport_id = ' . i($player['sport_id']) .
        ' AND team_id = ' . i($player['team_id']);
        
        $condition[3] = 'first_name = ' . s(i($player['first_name'])) .
        ' AND last_name = ' . s(i($player['last_name'])) .
        ' AND sport_id = ' . i($player['sport_id']);
        
        // if player is not matched, $player_id is null. db_m() is equal to 'first_name = "' . s($player['first_name']) . '" and last_name = "' . [...]
        $player_id = db_field('player', 'player_id', db_m($player, 'first_name, last_name, sport_id, team_id'));

        // before we create a new player, we need to check to see if the xml_team_id exists in the database already
        $xml_player_id = db_field('player', 'player_id', 'xmlteam_id = "' . i($player['xmlteam_id']) . '"');
        if ($xml_player_id == '') {
            
            // xmlteam_id does not exist.
            // let's look for the player by first_name, last_name, sport_id and team_id and update xmlteam_id
            $player_id = $xml_player_id;
            
        }
        
        if(is_null($player['xmlteam_id'])) {
            
            $playersMatched = db_rows('player', '*', $condition[2]);
            if(count($playersMatched) == 1){
                
                db_update('xmlteam_id', array_use(), $condition[2]);
                $player_id = $playersMatched[0]['player_id'];
                
            }else {
                
                $playersMatched = db_rows('player', '*', $condition[3]);
                if(count($playersMatched) == 1){
                    
                    db_update('xmlteam_id', array_use(), $condition[3]);
                    $player_id = $playersMatched[0]['player_id'];
                    
                }else if(count($playersMatched) == 0) {
                    
                    // new player, insert into database
                    $player_id = db_insert('player', $player);
                    
                }else{
                    
                    //ok. I assumed that this function took parameters similar ot this but managed to miss the description of this particular function in the comments.
                    dbg("Manually check multiple players with the same name: " $player['first_name'] . ' ' . $player['last_name'], $player)
                }
                
            }
            
        }
        

        if (is_null($player_id)) {
            
            // new player, insert into database
            $player_id = db_insert('player', $player);
            
        } else {
        	
            // existing player, update the parameters
            db_update('player', array_use($player, 'active, xmlteam_id, team_id, position_id'), 'player_id = ' . i($player_id));
            
        }
    }
}

?>
